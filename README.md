# HTML Dev App

> Environment and tools to develop HTML Pages using pugjs and Twitter Bootstrap 4.x.

**Tajid Yakub** <tajid.yakub@gmail.com> https://tajidyakub.com/

## Objective

This app is actually a boilerplate which on initial clone will provide us with a folder structure and modules needed in a entry point context to develop HTML Pages using Pug.js and Twitter Bootstrap, Fontawesome and Animate.css also available on extras.

As such a boilerplate, it is opinionated, while might not be useful for some, it could removes some required development tasks for some.

Go to Recommended Approach on Usage how to and start using this app.

## Paths

These directory structure is provided by the app along with some boilerplate files for you to later modify and extends while developing your HTML Pages.

### Asset files

### Template files (Pug)

## Stacks

These are utilized on providing the environment intended. 

- Node.js
- Webpack CLI, Loaders and Plugins
- Pug.js
- Twitter Bootstrap
- Fontawesome
- Animate CSS
- Editorconfig

### Webpack's loaders and plugins

You can view the version in the `package.json` all is using default release except for TextExtractPlugin which utilize `@next` when requiring.

- Pug loader
- SASS Loader, Node SASS, Style Loader and CSS Loader
- HTMLTemplate Webpack Plugin
- CleanWebpack Plugin

## Recomended Aproach

- Use Twitter Bootstrap theming via SASS variables.
