/**
 * This script will loads extra or additional modules.
 *
 * @since 1.0.0
 *
 * As of for now, these modules is required for then extracted
 * in to their own css files.
 *
 * - fontawesome 5.5.0
 * - animate.css
 */

import './assets/css/fontawesome.min.css';
import './assets/css/animate.min.css'