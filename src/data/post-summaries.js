module.exports = [
  {
    date: '10/10/2018',
    title: 'Bootstrap Theming with SASS Variables',
    image: 'https://picsum.photos/300/400/?image=566',
    category: 'Frontend',
    tags: [ 'Node.js','HTML', 'CSS','Webpack','SCSS', 'Javascript', 'jQuery', 'UI Framework']
  },
  {
    date: '07/10/2018',
    title: 'HTML Pages with Pug.js',
    image: 'https://picsum.photos/300/300/?image=512',
    category: 'Frontend',
    tags: [ 'HTML','Webpack','Javascript', 'jQuery', 'UI Framework', 'Node.js']
  },
  {
    date: '05/10/2018',
    title: 'Fun REST API Devs with Feathersjs',
    excerpt: 'Complete and powerful REST API and Websocket Server in no-time.',
    category: 'Backend',
    tags: [ 'REST API','Websocket','JWT', 'HTTP/s', 'App Framework', 'Node.js']
  }
]
