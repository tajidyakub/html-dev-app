import $ from 'jquery'
import 'popper.js'
import 'bootstrap'
import './assets/scss/style.scss'

window.$ = window.jQuery = $

/** Test the imported jquery in the browser. */
$(document).ready(function () {
  console.log('Hello, from HTML dev app');
})
